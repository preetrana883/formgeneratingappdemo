
import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  TextInput
} from 'react-native';
import Constants from '../../constants';

export default class FormCompo extends Component {

  render() {
    let { namePlaceholder, emailPlaceholder,
      ContactPlaceholder, websitePlaceholder,
      NameValue, EmailValue,
      ContactValue, WebsiteValue, onChangeTextName, onChangeTextEmail,
      onChangeTextContact, onChangeTextWebsite, dateOfBirthVale, onDobPress } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.flexDirStyle}>
          <TextInput
            style={styles.textInputStyle}
            placeholder={namePlaceholder}
            keyboardType={"default"}
            value={NameValue}
            onChangeText={onChangeTextName}
            placeholderTextColor={"gray"}
          />
          {!NameValue && <TouchableOpacity onPress={()=> alert("Name should only be of alphabets")}>
            <Image source={Constants.Images.user.Info} style={styles.infoImgStyle} />
          </TouchableOpacity>}
        </View>
        <View style={[styles.flexDirStyle, { marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4 }]}>
          <TextInput
            style={styles.textInputStyle}
            value={EmailValue}
            onChangeText={onChangeTextEmail}
            keyboardType={"email-address"}
            placeholder={emailPlaceholder}
            placeholderTextColor={"gray"}
          />
          {!EmailValue && <TouchableOpacity onPress={()=> alert("eg. abc@abc.com")}>
            <Image source={Constants.Images.user.Info} style={styles.infoImgStyle} />
          </TouchableOpacity>}
        </View>
        <View style={[styles.flexDirStyle, { marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4 }]}>
          <TextInput
            style={styles.textInputStyle}
            value={ContactValue}
            maxLength={10}
            onChangeText={onChangeTextContact}
            keyboardType={"phone-pad"}
            placeholder={ContactPlaceholder}
            placeholderTextColor={"gray"}
          />
          {!ContactValue && <TouchableOpacity onPress={()=> alert("Contact should be of 10 digits")}>
            <Image source={Constants.Images.user.Info} style={styles.infoImgStyle} />
          </TouchableOpacity>}
        </View>
        <View style={[styles.flexDirStyle, { marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4 }]}>
          <TextInput
            style={styles.textInputStyle}
            value={WebsiteValue}
            onChangeText={onChangeTextWebsite}
            keyboardType={"default"}
            autoCapitalize={'none'}
            placeholder={websitePlaceholder}
            placeholderTextColor={"gray"}
          />
          {!WebsiteValue && <TouchableOpacity onPress={()=> alert("website eg. abc.com")}>
            <Image source={Constants.Images.user.Info} style={styles.infoImgStyle} />
          </TouchableOpacity>}
        </View>
        <TouchableOpacity style={styles.dobStyle} onPress={onDobPress}>
          {dateOfBirthVale ? <Text style={styles.dobTextstyle}>{dateOfBirthVale}</Text>
            : <Text style={[styles.dobTextstyle, { color: "gray" }]}>Date of birth</Text>}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4,
    marginHorizontal: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
    alignItems: 'center',
    borderWidth: .9,
    borderRadius: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 2,
  },
  textInputStyle: {
    height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 5,
    borderColor: 'gray',
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 65,
    fontSize: 16,
  },
  dobStyle: {
    borderWidth: .5,
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,
    paddingVertical: 5,
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 4
  },
  dobTextstyle: {
    fontSize: 16,
    paddingHorizontal: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3,
  },
  flexDirStyle: {
    flexDirection: 'row',
    borderWidth: 1,
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 80,
    alignItems: "center",
    justifyContent: 'space-between',
    paddingHorizontal: Constants.BaseStyle.DEVICE_WIDTH / 100 * 3,
  },
  infoImgStyle: {
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 5,
    height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 5,
  }
});
