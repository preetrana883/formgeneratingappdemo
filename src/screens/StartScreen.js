
import React, {Component} from 'react';
import { TouchableOpacity, StyleSheet, Text, View} from 'react-native';
import Constants from '../constants'
// import { TouchableOpacity } from 'react-native-gesture-handler';

export default class StartScreen extends Component {
    static navigationOptions = {
        title: 'Start Screen',
        headerStyle: {
          backgroundColor: Constants.Colors.HeaderGreen,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
          fontSize: 18
        },
      };
    nxtPageFunc(){
        const{navigate}= this.props.navigation;
        navigate('FormPage');
    }
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.btnStyle} onPress={()=>this.nxtPageFunc()}>
            <Text style={styles.TextStyle}>{'Lets go to form screen'}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  btnStyle:{
      padding: Constants.BaseStyle.DEVICE_WIDTH/100*5,
      backgroundColor: Constants.Colors.PureBlack,
      borderRadius: Constants.BaseStyle.DEVICE_WIDTH/100*10,
  },
  TextStyle:{
      color: Constants.Colors.White,
      fontWeight: "500",
      fontSize: 18
  }
  
});