
import React, { Component } from 'react';
import {
  TouchableOpacity, StyleSheet, Text, View, Image,
  FlatList, DatePickerIOS, KeyboardAvoidingView, AsyncStorage
} from 'react-native';
import Constants from '../constants';
import FormCompo from '../components/common/FormCompo';
import FormSubmitButton from '../components/common/FormSubmitButton';
import FormTextInput from '../components/common/FormTextInput';
import { ScrollView } from 'react-native-gesture-handler';
import moment from "moment";
import Utility from '../utilities/Regex'
// import console = require('console');

export default class FormPage extends Component {
  static navigationOptions = {  // this is to give styling to the header 
    title: 'Form',              // of from screen from react navigation
    headerStyle: {
      backgroundColor: Constants.Colors.HeaderGreen,
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
      fontWeight: 'bold',
      fontSize: 18
    },
  };
  constructor(props) {
    super(props);
    this.state = {
      formList: [],
      name: '',
      email: '',
      contact: '',
      webSite: '',
      dob: '',
      text: '',
      chosenDate: new Date(),
      picker: false
    }
  }

  _keyExtractor = (item, index) => item.id; // this is used to give unique 
  // id's to the every element of the list
  componentWillMount() {
    console.log("componentDidmount***")
    this.retrieveData();
  }

  // function to store the formlist in the local storage i.e AsyncStorage
  storeData = async () => {
    try {
      await AsyncStorage.setItem('formList', JSON.stringify(this.state.formList));
    } catch (error) {
      // Error saving data
    }
  };

  // function to retreive the formlist data from local storage
  retrieveData = async () => {
    let newForm = [{
      name: "",
      email: "",
      contact: '',
      website: '',
      dob: ""
    }]
    
      const value = await AsyncStorage.getItem('formList');
      console.log("****formList****ASybch", value);
      if (value !== null) {
        this.setState({
          formList: JSON.parse(value)
        });
      } else if (value == null) {
        this.setState({
          formList: newForm
        })
      }
  };

  // function adds a new form after checking previous fields
  // all validations are done over here
  newFormAddFunc() {
    var shouldAddForm = true;
    let { formList } = this.state;
    for (i = 0; i < formList.length; i++) {
      if (formList[i].name == '') {
        shouldAddForm = false;
        alert("please enter name")
      } else if (Utility.validateString(formList[i].name)) {
        shouldAddForm = false;
        alert("Name should be of characters only")
      } else if (formList[i].email == '') {
        shouldAddForm = false;
        alert("Enter email")
      } else if (!Utility.validateEmail(formList[i].email)) {
        shouldAddForm = false;
        alert("invalid format eg- abc@abc.abc")
      } else if (formList[i].contact == '') {
        shouldAddForm = false;
        alert("Please enter contact number")
      } else if (formList[i].contact.length < 10) {
        shouldAddForm = false;
        alert("contact should be of 10 digits")
      } else if (formList[i].website == '') {
        shouldAddForm = false;
        alert("Please enter website")
      } else if (!Utility.validateURL(formList[i].website)) {
        shouldAddForm = false;
        alert("invalid format eg- https://abc.com")
      } else if (formList[i].dob == '') {
        shouldAddForm = false;
        alert("Please select date of birth")
      } else {
        var newData = [...formList, {
          name: '',
          email: '',
          contact: '',
          website: '',
          dob: '',
          index: null
        }];
        this.setState({ formList: newData })
      }
    }

    for (i = 0; i < formList.length; i++) {
      if (formList[i].name == '' ||
        formList[i].email == '' ||
        formList[i].contact == '' ||
        formList[i].website == '' ||
        formList[i].dob == ''
      ) {
        shouldAddForm = false;
      }
    }
  }

  // function called when submit button is pressed 
  //form will be submitted to the local storage if it passes all validations
  onSumitBtnPress() {
    let { formList } = this.state;
    var shouldAddForm = true;
    for (i = 0; i < formList.length; i++) {
      if (formList[i].name == '') {
        shouldAddForm = false;
        alert("please enter name")
      } else if (Utility.validateString(formList[i].name)) {
        shouldAddForm = false;
        alert("Name should be of characters only")
      } else if (formList[i].email == '') {
        shouldAddForm = false;
        alert("Enter email")
      } else if (!Utility.validateEmail(formList[i].email)) {
        shouldAddForm = false;
        alert("invalid format eg- abc@abc.abc")
      } else if (formList[i].contact == '') {
        shouldAddForm = false;
        alert("Please enter contact number")
      } else if (formList[i].contact.length < 10) {
        shouldAddForm = false;
        alert("contact should be of 10 digits")
      } else if (!Utility.validateURL(formList[i].website)) {
        shouldAddForm = false;
        alert("invalid format eg- https://abc.com")
      } else if (formList[i].dob == '') {
        shouldAddForm = false;
        alert("Please select date of birth")
      }
    }
    if (shouldAddForm) {
      this.storeData();
      return alert("submitted")
    }

  }

  // func will be called for every iteration
  // FormCompo is custom component which is made in separate folder
  renderItemFunc(data) {
    return (
      <FormCompo
        namePlaceholder={"Name"}
        emailPlaceholder={'Email'}
        ContactPlaceholder={"contact"}
        websitePlaceholder={'Web site'}
        DOBPlaceholder={'Date of birth'}
        NameValue={data.item.name}
        EmailValue={data.item.email}
        ContactValue={data.item.contact}
        WebsiteValue={data.item.website}
        dateOfBirthVale={data.item.dob ? moment(new Date(data.item.dob)).format('ll') : ''}
        onChangeTextName={(name) => this.changeValueTextFunc(name, data, data.index, 'name')}
        onChangeTextEmail={(email) => this.changeValueTextFunc(email, data, data.index, "email")}
        onChangeTextContact={(contact) => this.changeValueTextFunc(contact, data, data.index, "contact")}
        onChangeTextWebsite={(website) => this.changeValueTextFunc(website, data, data.index, "website")}
        onDobPress={() => this.changeDateOfBirth(data, data.index)}
      />
    )
  }

  //function will be called whenever a user tries to edit fields
  //i.e name, email, contact, and website etc.
  changeValueTextFunc = (stateName, data, index, type) => {
    let { formList } = this.state;
    switch (type) {
      case "name":
        formList[index].name = stateName;
        this.setState({ name: stateName })
        break;

      case "email":
        formList[index].email = stateName;
        this.setState({ email: stateName })
        break;

      case "contact":
        formList[index].contact = stateName;
        this.setState({ contact: stateName })
        break;

      case "website":
        formList[index].website = stateName;
        this.setState({ webSite: stateName })
        break;
      // default:
      //   // code block
    }

  }

  //function will be called whenever a user tries to edit date of birth feild
  changeDateOfBirth(data, index) {
    return (
      this.setState({ picker: true, index, chosenDate: data.item.dob ? new Date(data.item.dob) : new Date() })
    )
  }

  // this function will set the value in state i.e. chosenDate
  setDate(data) {
    console.log("picker   data ****", data)
    this.setState({ chosenDate: data });
  }

  // this function will set the dob of the user
  setPickerValueFunc() {
    let { formList, index, chosenDate } = this.state;
    formList[index].dob = chosenDate
    this.setState({ picker: false })
  }

  //default render function 
  render() {
    console.log("this.state.formList*******", this.state.formList);
    let { formList, picker } = this.state;
    let arrFormList = [...this.state.formList]
    return (
      <View style={styles.container}>
        <ScrollView>
          <KeyboardAvoidingView style={styles.container} behavior="height" enabled>
            <TouchableOpacity style={styles.addImgView} onPress={() => this.newFormAddFunc()}>
              <Image style={styles.imgAddStyle} source={Constants.Images.user.Add} />
            </TouchableOpacity>
            <View style={styles.listViewStyle}>
              <FlatList
                data={arrFormList}
                extraData={this.state.formList}
                keyExtractor={this._keyExtractor}
                renderItem={(data) => this.renderItemFunc(data)}
              />
            </View>
            <View style={styles.submitBtnView}>
              {this.state.formList.length > 0 && <FormSubmitButton text={"SUBMIT"}
                _Press={() => this.onSumitBtnPress()}
                buttonStyle={{ marginTop: 20, marginBottom: 20 }} />}
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
        {picker && <DatePickerIOS
          date={this.state.chosenDate}
          maximumDate={new Date()}
          mode={"date"}
          onDateChange={(data) => this.setDate(data)}
        />}
        {picker && <TouchableOpacity style={styles.okTextViewStyle}
          onPress={() => this.setPickerValueFunc()}>
          <Text style={styles.okTextStyle}>OK</Text>
        </TouchableOpacity>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  addImgView: {
    position: 'absolute',
    right: Constants.BaseStyle.DEVICE_WIDTH / 100 * 6,
    top: Constants.BaseStyle.DEVICE_WIDTH / 100 * 6,
  },
  imgAddStyle: {
    width: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8,
    height: Constants.BaseStyle.DEVICE_WIDTH / 100 * 8,
  },
  listViewStyle: {
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
  },
  submitBtnView: {
    alignItems: 'center'
  },
  okTextViewStyle: {
    alignItems: 'center'
  },
  okTextStyle: {
    fontSize: 16,
    color: Constants.Colors.PureBlack,
    marginBottom: 20
  }
});