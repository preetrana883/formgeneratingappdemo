"use strict"

let colors = {
    Transparent: 'transparent',
    White:"#ffffff",
    Gray: "#737373",
    Black:'#212123',
    PureBlack : "#000000",
    Green: '#009a0e',
    HeaderGreen: "#1e980a",
};

module.exports =  colors;
