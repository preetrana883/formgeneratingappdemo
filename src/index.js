import React from "react";
import { View, Text } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import StartScreen from './screens/StartScreen'
import FormPage from './screens/FormPage'


const AppNavigator = createStackNavigator({
  StartScreen: {
    screen: StartScreen
  },
  FormPage: {
    screen: FormPage
  },

});

export default createAppContainer(AppNavigator);